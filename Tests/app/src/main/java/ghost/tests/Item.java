package ghost.tests;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ghost on 26.10.2015.
 */
public class Item implements  Parcelable{
    private String header1;
    private String header2;
    private String mainContent;
    public Item (String header1, String header2, String mainContent){
        this.header1 = header1;
        this.header2 = header2;
        this.mainContent = mainContent;
    }
    protected Item(Parcel in) {
        header1 = in.readString();
        header2 = in.readString();
        mainContent = in.readString();
    }

    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(header1);
        dest.writeString(header2);
        dest.writeString(mainContent);
    }

    public String getHeader() {
        return header1;
    }

    public String getInfoHeader() {
        return header2;
    }

    public String getText() {
        return mainContent;
    }
}
