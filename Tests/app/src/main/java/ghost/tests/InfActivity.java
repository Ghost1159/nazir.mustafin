package ghost.tests;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by Ghost on 27.10.2015.
 */
public class InfActivity extends AppCompatActivity{


        private TextView header1;
        private TextView header2;
        private TextView mainContent;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_inf);
            header1 = (TextView) findViewById(R.id.header1);
            header2 = (TextView) findViewById(R.id.header2);
            mainContent = (TextView) findViewById(R.id.mainContent);
            Item item = getIntent().getParcelableExtra("parcel");
            header1.setText(item.getHeader());
            header2.setText(item.getInfoHeader());
            mainContent.setText(item.getText());
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
}
