package ghost.tests;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        setupRecyclerView(mRecyclerView);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new RecyclerViewAdapter(Items.getList()));
        Log.d("sLog", Items.getList().get(0).getHeader());
    }


    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolderItem> {
        private List<Item> items;

        public RecyclerViewAdapter(List<Item> items) {
            this.items = items;
        }

        public class ViewHolderItem extends RecyclerView.ViewHolder {

            public final View mView;
            public final TextView header1;
            public final TextView header2;
            public final TextView mainContent;

            public ViewHolderItem(View view) {
                super(view);
                mView = view;
                header1 = (TextView) view.findViewById(R.id.header1);
                header2 = (TextView) view.findViewById(R.id.header2);
                mainContent = (TextView) view.findViewById(R.id.mainContent);
            }
        }

        @Override
        public ViewHolderItem onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
            return new ViewHolderItem(v);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        @Override
        public void onBindViewHolder(ViewHolderItem viewHolderItem, int i) {
            final Item item = items.get(i);
            Log.d("sLog", "from Items.getList: " + Items.getList().get(i).getHeader());
            Log.d("sLog", "from items.get(i)  " + item.getHeader());
            Log.d("sLog", "i: " + String.valueOf(i));

            viewHolderItem.header1.setText(item.getHeader());
            viewHolderItem.header2.setText(item.getInfoHeader());
            viewHolderItem.mainContent.setText(item.getText());

            viewHolderItem.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, InfActivity.class);
                    intent.putExtra("parcel", item);
                    startActivity(intent);
                }
            });
        }


    }
}
