package ghost.calculator;

import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private String answer;
    private String exp="";
    public int Calculate(String str){
        String vl1="";
        String vl2="";
        boolean bln = true;
        int answer;
        int index = str.indexOf("+");
        if (index == -1){
            index = str.indexOf("-");
            bln = false;
        }
        for(int i = 0; i < index; i++){
            vl1+=str.charAt(i);
        }
        for(int i = index + 1; i <str.length(); i++){
            vl2+=str.charAt(i);
        }
        answer = Integer.parseInt(vl1);
        if (bln){
            answer+= Integer.parseInt(vl2);
        }
        else{
                answer-= Integer.parseInt(vl2);
        }
        return answer;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView main = (TextView) findViewById(R.id.in);
        main.setText("0");
        Button[] btns ={
                (Button) findViewById(R.id.i0),
                (Button) findViewById(R.id.i1),
                (Button) findViewById(R.id.i2),
                (Button) findViewById(R.id.i3),
                (Button) findViewById(R.id.i4),
                (Button) findViewById(R.id.i5),
                (Button) findViewById(R.id.i6),
                (Button) findViewById(R.id.i7),
                (Button) findViewById(R.id.i8),
                (Button) findViewById(R.id.i9),
                (Button) findViewById(R.id.plus),
                (Button) findViewById(R.id.minus),
                (Button) findViewById(R.id.equally),
                (Button) findViewById(R.id.clear)
        };

        btns[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"0";
                main.setText(exp);
            }
        });
        btns[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"1";
                main.setText(exp);
            }
        });
        btns[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"2";
                main.setText(exp);
            }
        });
        btns[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"3";
                main.setText(exp);
            }
        });
        btns[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"4";
                main.setText(exp);
            }
        });
        btns[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"5";
                main.setText(exp);
            }
        });
        btns[6].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"6";
                main.setText(exp);
            }
        });
        btns[7].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"7";
                main.setText(exp);
            }
        });
        btns[8].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"8";
                main.setText(exp);
            }
        });
        btns[9].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"9";
                main.setText(exp);
            }
        });
        btns[10].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"+";
                main.setText(exp);
            }
        });
        btns[11].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = exp+"-";
                main.setText(exp);
            }
        });
        btns[12].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp = Integer.toString(Calculate(exp));
                main.setText(exp);
            }
        });
        btns[13].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp="";
                main.setText(exp);
            }
        });

        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
